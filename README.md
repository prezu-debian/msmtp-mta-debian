# msmtp-mta-debian

Ansible Role that enables Mail Transport Agent (MTA) on a Debian system using
[msmtp-mta](https://packages.debian.org/bullseye/msmtp-mta).

# Pipeline Status

[![pipeline status](https://gitlab.com/prezu-debian/msmtp-mta-debian/badges/main/pipeline.svg)](https://gitlab.com/prezu-debian/msmtp-mta-debian/-/commits/main)

# Role Variables

* `auth`: `on`, `off`, or an authentication method. To see all available
  methods, run `msmtp --version`.
* `smtp_user`: If you're using Authentication, this variable is your account's
  SMTP username.
* `smtp_pass`: As above, but SMTP password.
* `smtp_host`: SMTP server of your mail account.
* `smtp_from`: An envelope's `From` email address. This is probably going to
  have to align with you email account's email address.
* `enforce_smtp_from`: If set to yes (default is no) it'll enforce "from" to
  match `smtp_from`.
* `smtp_port`: A TCP port for yout SMTP connection. E.g. `"25"`, or `"587"`.
* `tls_enabled`: Set it to `yes`, if your account is using TLS, `no`
  otherwise.
* `tls_starttls`: Set it to `yes`, if your account is using STARTTLS, `no`
  otherwise.
* `aliases`: A list of `user`/`alias` pairs to define the aliases for your
  system. See the examples below.

# Examples

You can use Ansible Galaxy to install this role. E.g. add the following to
your `requirements.yaml`:

```yaml
- src: git+https://gitlab.com/prezu-debian/msmtp-mta-debian
  version: v1.0.0
  name: msmtp-mta-debian
```

and then run:

```shell
$ ansible-galaxy install -r requirements.yaml
```

After that you can use it in your playbook. E.g. simple TLS with STARTTLS
and plain authentication:

```yaml
    - role: msmtp-mta-debian
      vars:
        auth: "plain"
        smtp_user: "{{ my_smtp_user }}"
        smtp_pass: "{{ my_smtp_pass }}"
        smtp_host: "smtp.example.com"
        smtp_from: "your.email.address@example.com"
        enforce_smtp_from: "yes"
        smtp_port: "587"
        tls_enabled: "yes"
        tls_starttls: "yes"
        aliases:
          - user: root
            alias: "your.email.address+root@example.com"
          - user: cron
            alias: "your.email.address+cron@example.com"
          - user: default
            alias: "your.email.address+everything.else@example.com"

```
# License

GPLv3
